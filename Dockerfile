FROM ubuntu:latest

WORKDIR /app
RUN apt-get update -y && \
    apt-get install -y python3 python3-pip git nano && \
    git clone https://gitlab.cern.ch/grigolet/pyhvwrapper.git . && \
    mkdir /configs

# We copy just the requirements.txt first to leverage Docker cache
COPY ./requirements.txt /app/requirements.txt


RUN pip3 install -r requirements.txt

COPY . /app
COPY configs.example.json /configs/configs.json

RUN chmod -R 775 /app
RUN chown -R 1001:root /app

USER 1001
EXPOSE 5000

# ENTRYPOINT ["gunicorn", "-b", "0.0.0.0:8081", "wsgi:app"]
ENTRYPOINT ["python3", "server.py"]