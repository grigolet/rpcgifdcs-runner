# rpcgifdcs-runner

Web server that starts, adjust, stop, restart the monitoring of the chambers.
The server reacts to the following api requests:

* `POST /monitor/alert`: endpoint for grafana alerts. It checks for the status ('alerting', 'no_data', 'ok'), the type of alert and decides which action should be taken
* `POST /monitor/set`: endpoint for starting monitoring. It starts the chambers to the desired voltage. Data should be passed as a json in the following configuration.
    - `channel` must be a number
    - `power` can be either 'on' or 'off'
    - `voltage` is the voltage, corrected for the pressure inside the bunker
    - `effective` can be used to correct the applied voltage for the pressure and the temperature, by the formula
    - `I0Set` is the limit in the current and can be either a number (in uA) or set to "auto"
  for letting the program choosing the proper limit. The limit will be set once the currents reach the voltage defined and a limit of 5 + Imon + 0.05 * Imon will be applied
```json
{
    "slot": 3,
    "settings": [
        {"channel": 0, "power": "on", "voltage": 11600, "effective": true, "I0Set": 150 },
        {"channel": 1, "power": "off", "voltage": 11600, "effective": false, "I0Set": "auto" },
    ]
}
```
* `POST /monitor/start?voltage=11600&I0Set=150`: endpoint that will automatically starts all the chambers at the effective voltage defined by the paraeter `voltage` with the current limit defined by `I0Set`
* `POST /monitor/stop`: endpoint that will shutdown the chambers.
* `POST /monitor/standby?voltage=6500`: endpoint that will put the chambers into standby 
