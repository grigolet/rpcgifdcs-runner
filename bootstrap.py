import json

def create_run_parameters_file(path='run_parameters.json'):
    data = {
        "start": {
            "settings":  [
                {
                    "slot": 2,
                    "channel_configs": [
                        {"slot": 2, "channel": 0, "Pw": "on", "voltage": 9000, "I0Set": 280, "effective": True},
                        {"slot": 2, "channel": 1, "Pw": "on", "voltage": 9000, "I0Set": 280, "effective": True},
                        {"slot": 2, "channel": 2, "Pw": "on", "voltage": 9000, "I0Set": 280, "effective": True}
                    ]
                }
                
            ]
        },
        "standby": {
            "settings":  [
                {
                    "slot": 2,
                    "channel_configs": [
                        {"slot": 2, "channel": 0, "Pw": "on", "voltage": 6500, "I0Set": 10, "effective": True},
                        {"slot": 2, "channel": 1, "Pw": "on", "voltage": 6500, "I0Set": 10, "effective": True},
                        {"slot": 2, "channel": 2, "Pw": "on", "voltage": 6500, "I0Set": 10, "effective": True}
                    ]
                }
                
            ]
        },
        "stop": {
            "settings":  [
                {
                    "slot": 2,
                    "channel_configs": [
                        {"slot": 2, "channel": 0, "Pw": "off", "voltage": 15, "I0Set": 1, "effective": True},
                        {"slot": 2, "channel": 1, "Pw": "off", "voltage": 15, "I0Set": 1, "effective": True},
                        {"slot": 2, "channel": 2, "Pw": "off", "voltage": 15, "I0Set": 1, "effective": True}
                    ]
                }
                
            ]
        }
    }
    with open(path, 'w') as json_file:
        json.dump(data, json_file)