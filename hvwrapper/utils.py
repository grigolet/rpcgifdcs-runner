from enum import IntEnum
from ctypes import *


class CtypesEnum(IntEnum):
    """A ctypes-compatible IntEnum superclass. Used for enum in C.
    Usage: create a class that inherits from this class. Example:
    class ENUM_TYPE_t(CTypesEnum):
        SY1527 = 0
        SY2527 = 1
        SY3527 = 2
    
    Access a value with the syntax `ENUM_TYPE_t.SY2527`
    """
    @classmethod
    def from_param(cls, obj):
        return int(obj)


class CAENHV_SYSTEM_TYPE_t(CtypesEnum):
    """Enum class for module types"""
    SY1527 = 0
    SY2527 = 1
    SY4527 = 2
    SY5527 = 3
    N568 = 4
    V65XX = 5
    N1470 = 6
    V8100 = 7
    N568E = 8
    DT55XX = 9
    FTK	 = 10
    DT55XXE = 11
    N1068 = 12
    
error_codes = {
    0: 'Command wrapper correctly executed',
    1: 'Error of operatived system',
    2: 'Write error in communication channel',
    3: 'Read error in communication channel',
    4: 'Time out in server communication',
    5: 'Command Front End application is down',
    6: 'Communication with system not yet connected by a Login command',
    7: 'Communication with a not present board/slot',
    8: 'Communication with RS232 not yet implemented',
    9: 'User memory not sufficient',
    10: 'Value out of range',
    11: 'Execute command not yet implemented',
    12: 'Get Property not yet implemented',
    13: 'Set Property not yet implemented',
    14: 'Property not found',
    15: 'Execute command not found',
    16: 'No System property',
    17: 'No get property',
    18: 'No set property',
    19: 'No execute command',
    20: 'Device configuration changed',
    21: 'Property of param not found',
    22: 'Param not found',
    23: 'No data present',
    24: 'Device already open',
    25: 'To Many devices opened',
    26: 'Function Parameter not valid',
    27: 'Function not available for the connected device',
    0x1001: 'Device already connected',
    0x1002: 'Device not connected',
    0x1003: 'Operating system error',
    0x1004: 'Login failed ',
    0x1005: 'Logout failed',
    0x1006: 'Link type not supported',
    0x1007: 'Login failed for username/password ( SY4527 / SY5527 )'
}

param_types = {
    'V0Set': c_float,
    'I0Set': c_float,
    'V1Set': c_float,
    'I1Set': c_float,
    'Rup': c_float,
    'RDWn': c_float,
    'Trip': c_float,
    'VMon': c_float,
    'IMon': c_float,
    'Status': c_uint,
    'Pw': c_bool,
    'Pon': c_bool,
    'PDWn': c_bool
}

status_types = {
    0: 'Off',
    1: 'On',
    3: 'Ramp up',
    5: 'Ramp down',
    9: 'Overcurrent',
    17: 'Overvoltage',
    33: 'Undervoltage',
    65: 'External trip',
    129: 'Max V',
    257: 'External disable',
    513: 'Internal trip',
    1025: 'Calibration error',
    2049: 'Unplugged',
    # TODO: continue
}