from flask import Flask, request, jsonify
import json
from HVWrapper import HV
from utils import error_codes
from threading import Timer
from influxdb import InfluxDBClient
import time
import traceback
import logging
from multiprocessing import Process, Value
from time import sleep
from bootstrap import create_run_parameters_file
from flask.logging import default_handler
import os

# define logger
logger = logging.getLogger()
logger.addHandler(default_handler)
logger.setLevel(int(os.environ.get('LOGGING_LEVEL', logging.INFO)))

# Create run_parameters.json dynamically
create_run_parameters_file()

# Define webserver global object
app = Flask(__name__)

# load server configurations, such as db and hv module
with open('/configs/configs.json') as f:
    configs = json.load(f)

# Define a shared memory variables to be used
# between the runner process and the web server app
# mode can be 0, 1 or 2
mode = Value('d', 2)
# mode map maps the value of mode to a string for
# better readability
mode_map = {
    0: 'start',
    1: 'standby',
    2: 'stop'
}

# Instantiate the client for influxdb. Used
# to continuously read the pressure and the 
# current for voltage correction
client = InfluxDBClient(**configs['db'])
# Default reference value for correction
p0 = configs['default']['p0']
t0 = configs['default']['t0']

# DIP subscription from which the database should
# query for getting data
sub_pressure = configs['default']['sub_pressure']
sub_temperature = configs['default']['sub_temperature']

# String defining the query to be used to retrieve data 
# of pressure and temperature
query_string = """
SELECT "Float_value" 
FROM "dip"
WHERE "sub" = '{}'
ORDER by desc
LIMIT 1
"""

# simple dict to map off and on values between
# integer (used by hv module) and string for 
# better readability
power_map = {
    'off': 0,
    'on': 1
}

# Configs for instantiate the HV object
hv_configs = configs['hv']

# Instantiate a global hv object
hv = HV(ip_address=hv_configs['ip_address'],
            sys_type=hv_configs['sys_type'],
            user=hv_configs['user'],
            password=hv_configs['password'],
            lib_path='hvwrapper/libcaenhvwrapper.so.5.82')

result = hv.connect()
logger.info(hv_configs)
logger.info(f'Result: {result}')

def get_point_from_db(sub):
    """Return a tuple containing a time and the value from
    a dip subscrption. The function queries to influxdb"""
    result = client.query(query_string.format(sub)).raw
    time, value = result['series'][0]['values'][0]
    return time, value

def reset_connection():
    """Helper function to reset the connection so that
    one can be sure that the hv object doesn't go on timeout"""
    hv.disconnect()
    result = hv.connect()
    return result

def set_I0Set(settings):
    """This functions is setting the current limit based on 
    the settings applied. If the limit is a number it will set
    a number. If the limit in the params is set to auto it
    will set it to the current value plus 5uA and 10%."""
    for setting in settings:
        slot = setting['slot']
        channels = [ch_config['channel'] for ch_config in setting['channel_configs']]
        current_currents = hv.get_param(slot, channels, param='IMon')
        for channel_config in setting['channel_configs']:
            channel = channel_config['channel']
            if channel_config['I0Set'] == 'auto':
                current = current_currents[channel]
                # TODO: remove hardcoded formula
                set_current = 5 + current * 1.1
            else:
                set_current = channel_config['I0Set']
        
            logger.info(f'I0Set for channel {channel}: {set_current}uA')
            reset_connection()
            hv.set_param(slot, channel, param='I0Set', value=set_current)


def get_global_params(path='run_parameters.json'):
    """Function to retrieve the json representing the 
    global parameters"""
    with open(path, 'r') as json_file:
            json_configs = json.load(json_file)
    return json_configs


def update_run_params(action, params, path='run_parameters.json'):
    """Update the parameters in the json file regarding 
    a particular action. The action can be 'start', 'stop', 'standby'"""
    current_params = get_global_params()
    new_params = current_params
    new_params[action] = params
    current_params[action] = params
    with open(path, 'w') as json_file:
        json.dump(new_params, json_file)


def runner(mode, sleep_time=2):
    """Process that runs continuosly on background. It sleeps every sleep second
    to avoid to overload the hv module of function calls.
    """
    # This variable is used to get track of the eventual change of action.
    # In that case the function to set the current limit will be called.
    # Example: from 'stop' to 'start' -> set_current_limit()
    # Example 2: from 'start' to 'stop' -> set_current_limit()
    current_action = ''
    current_global_params = {}
    # Instantiate the local hv object
    # Instantiate a global hv object
    hv = HV(ip_address=hv_configs['ip_address'],
                sys_type=hv_configs['sys_type'],
                user=hv_configs['user'],
                password=hv_configs['password'],
                lib_path='hvwrapper/libcaenhvwrapper.so.5.82',
                logger=logger)

    result = hv.connect()
    logger.info(hv_configs)
    logger.info(f'Result: {result}')
    # Main background loop
    while True:
        # Keep connection alive to be sure it's not on timeout
        hv.get_map()
        # the action can be 'start', 'stop', or 'standby'
        action = mode_map[mode.value]
        # Every cycle of the loop read the parameters from file
        # to see if they are updated
        global_params = get_global_params()
        if current_global_params != global_params or action != current_action:
            should_set_I0Set = True
            current_global_params = global_params
            current_action = action
        run_params = global_params[action]
        # Take the slot param and the settings
        settings = run_params['settings']

        # If the action is start on standby we need to continuously
        # adjust the voltage to apply and effective voltage
        if action in ['start', 'standby']:
            for setting in settings:
                # For each setting item there is a unique slot
                # number and a list of channel configuration.
                # This is done in case new chambers on new slots
                # will be added
                slot = setting['slot']
                channels = [ch_config['channel'] for ch_config in setting['channel_configs']]
                for channel_config in setting['channel_configs']:
                    channel = channel_config['channel']
                    # If the action is standby and the channel is off because
                    # it's not intended to be used don't change skip the computation
                    # of the voltage to apply
                    if action == 'standby':
                        pw_status = hv.get_param(slot=slot, channels=channel, param='Pw')[channel]
                        if pw_status == 0:
                            logger.info(f'Recevied standby command but channel {channel} is off. Skipping.')
                            continue
                    # Get pressure and temperature from database
                    time, pressure = get_point_from_db(sub_pressure)
                    time, temperature = get_point_from_db(sub_temperature)
                    correction = pressure / p0 * (t0 + 273.15) / (temperature + 273.15) if channel_config['effective'] == True else 1
                    # v_app is the voltage to be applied to get an effective v_set
                    v_set = channel_config['voltage']
                    v_app =  int(correction * v_set)
                    logger.debug('Pressure %f, Temperature %f, V set %f, correction %f, Vapp %f, Sub pressure %s, Sub temperature %s', pressure, temperature, v_set, correction, v_app, sub_pressure, sub_temperature)
                    
                    # Set current limit to global config objects defined in the configs
                    I0Set = channel_config['I0Set']
                    if I0Set == 'auto': I0Set = configs['default']['I0Set_start']
                    # Set the power to the value present in the configs.
                    # The value can be either off or on depending on the channels
                    pw_status = power_map[channel_config['Pw']]
                    result = hv.set_param(slot=slot, channels=channel, param='Pw', value=pw_status)
                    if result != 0: logger.error(f'Setting pw: Channel: {channel}, V0Set, Code: {result} = {error_codes[result]}')
                    # Now set the value of the voltage
                    result = hv.set_param(slot=slot, channels=channel, param='V0Set', value=v_app)
                    if result != 0: logger.error(f'Setting V0Set: Channel: {channel}, V0Set, Code: {result} = {error_codes[result]}')
                    
                    # sleep a fraction of seconds so that the status can be updated in the hv module
                    sleep(sleep_time)
                    # Also raise the current limits if the chamber is in ramp-up to avoid it tripping
                    status = hv.get_param(slot, channel, 'Status')[channel]

                    # If there has been a change in the action the chambers can either
                    # 1. from 'stop' to 'start' -> set_current_limit(high_value) is desired
                    # 2. from 'start' to 'stop' -> set_current_limit(high_value) not desired but safe
                    # 3. from 'standby' to 'start' -> set_current_limit(high_value) is desired
                    # 4. from 'start' to 'standby' -> set_current_limit(high_value) not desired but safe
                    if current_action != action or status == 'Ramp up':
                        result = hv.set_param(slot=slot, channels=channel, param='I0Set', value=I0Set)
                        if result != 0: logger.error(f'Setting I0Set: Channel: {channel}, I0Set, Code: {result} = {error_codes[result]}')
                        
                # Get the status of the channels to check if they are either ramping up or 
                # ramping down
                sleep(sleep_time)
                status_channels = hv.get_param(slot, channels, 'Status').values()
                # wait until there is still at least one of the channel ramping
                while any((status == 'Ramp up' or status == 'Ramp down' for status in status_channels)):
                    sleep(sleep_time)
                    status_channels = hv.get_param(slot, channels, 'Status').values()

                # if there is a status that is not ramping and it's not On
                # send a warning message
                if any((status != 'On' for status in status_channels)):
                    logger.warning(f'Status: {status_channels}. Trying reset connection')
                    result_disconnect = hv.disconnect()
                    result_connect = hv.connect()
                    logger.info(f'Resetting connection resulted in code: disconnect: {result_disconnect}, connect: {result_connect}')
                    logger.info('Querying channels again')
                    status_channels = hv.get_param(slot, channels, 'Status').values()
                    logger.info(f'Status of new channels: {status_channels}')


                # If there is a new action or the parameters changd then set the new current limits after ramping up/down
                if should_set_I0Set:
                    set_I0Set(settings)
                    should_set_I0Set = False
            
            current_action = action

        # if the action is 'stop' nothing will happen. The loop will
        # silently continue to do nothing

        # sleep the desired amount of time before starting a new cycle
        sleep(sleep_time)

@app.route('/monitor/start', methods=['POST'])
def start():
    app.logger.info('Got start request. Preparing')
    # For security reset the connection
    reset_connection()
    # Parameters sent with the request
    params = request.get_json()
    # Parameters from json file
    global_params = get_global_params()
    # Parameters for the particular run
    run_params = global_params['start']
    # Merge the run pamereters with the one of the request
    applied_params = {**run_params, **params}
    # Update the json_file with the new merged parameters
    app.logger.info('applied parameters: ')
    app.logger.info(applied_params)
    update_run_params('start', applied_params)
    # Set the value of mode to start so that the runner
    # will be triggered
    mode.value = 0
    # Return the response to the request with the updated
    # parameters
    return jsonify(applied_params)

@app.route('/monitor/standby', methods=['POST'])
def standby():
    # Similar to start() function
    try:
        reset_connection()
        params = request.get_json()
        global_params = get_global_params()
        run_params = global_params['standby']
        applied_params = {**run_params, **params}
        update_run_params('standby', applied_params)
        mode.value = 1
        return jsonify(applied_params)
    except Exception as e:
        traceback.print_exc()
        return traceback.format_exc()


@app.route('/monitor/stop', methods=['POST'])
def stop():
    # Similar to standby function
    reset_connection()
    params = request.get_json()
    global_params = get_global_params()
    # This piece is commented as it started chambers when status is okay
    # I prefer to manually starts the chambers

    # if 'state' in params and params['state'] == 'ok':
    #     # It means that a previous alert got back to status okay.
    #     # In this case we need to switch on the chambers again
    #     logger.info(f'Starting chambers again. State {params["state"]}')
    #     run_params = global_params['start']
    #     update_run_params('start', run_params)
    #     mode.value = 0
    #     return jsonify({
    #         **params,
    #         'info': 'chambers switched on',
    #         **run_params
    #     })
    
    run_params = global_params['stop']
    applied_params = {**run_params, **params}
    update_run_params('stop', applied_params)
    # Trigger the mode so that the runner will run without doing
    # anything
    mode.value = 2
    # Switch off the chambers
    for setting in applied_params['settings']:
        slot = setting['slot']
        for channel_config in setting['channel_configs']:
            channel = channel_config['channel']
            result = hv.set_param(slot=slot, channels=channel, param='Pw', value=0)
            app.logger.info(f'Switching off channel {channel}')
            if result != 0: app.logger.error(f'Channel: {channel}, V0Set:, Code: {result} = {error_codes[result]}')

    return jsonify({
        **params,
        'info': 'chambers swtiched off',
        **run_params
    })


@app.route('/monitor/status')
def status():
    return mode_map[mode.value]


@app.route('/monitor')
def index():
    return 'pong'

if __name__ == '__main__':
    sleep_time = 2
    runner_process = Process(target=runner, args=(mode, sleep_time))
    runner_process.start()
    app.run(host='0.0.0.0')