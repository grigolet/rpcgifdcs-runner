from flask import Flask, request, jsonify, Response
import time
from flask_executor import Executor
app = Flask(__name__)
executor = Executor(app)

app.config['EXECUTOR_TYPE'] = 'thread'
app.config['EXECUTOR_MAX_WORKERS'] = 10

def switch_on(ch, test):
    app.logger.info(f'Switching on chamber on channel {ch}, {test}')
    time.sleep(2 - ch + 1)
    app.logger.info(f'Swtiched on on chamber on channel {ch}')
    return True

@app.route('/start')
def start():
    ch = 0
    test = 3
    executor.submit_stored(f'ch{ch}', switch_on, ch, test)
    ch = 1
    test = 's'
    executor.submit_stored(f'ch{ch}', switch_on, ch, test)
    return Response(status=200)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=7000, debug=True)