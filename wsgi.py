from server import app, runner
import logging

if __name__ == "__main__":
    sleep_time = 2
    runner_process = Process(target=runner, args=(mode, sleep_time))
    runner_process.start()
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)
    app.run(debug=True)